# RAE359 #
## Distributed systems #

### Links:
* [Safari Books Online](https://www.safaribooksonline.com/)
* [EDX](http://edx.etf.rtu.lv)
* [TSC EDX LV](https://tsc.edx.lv)
* [Distributed Systems in One lesson](https://www.safaribooksonline.com/library/view/distributed-systems-in/9781491924914/)

## Video

https://youtu.be/s3xmsAYH3t0 Rinalds Lācis 151REB013